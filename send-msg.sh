#!/bin/bash

# Store the script path in a variable
DIR="$( cd -- "$(dirname "$( readlink -f "$0" )")" >/dev/null 2>&1 ; pwd -P )"

# Load config
source "${DIR}/config/bot.cfg"

# Directory check
[ ! -d "${DIR}/send" ] && echo "[!] Error: could not find 'send' directory" && exit 1

# Make 'sent' directory
mkdir -p "${DIR}/sent"

for file in "${DIR}/"send/*; do

	[ ! -f "$file" ] && continue

	file="$( basename "$file" )"

	# Check file is not in the 'sent' directory
	[ -f "${DIR}/sent/${file}" ] && \
		echo "[!] File already sent: '$file'" && \
		rm "${DIR}/send/${file}" && \
		continue

	# Get message
	msg="$( cat "${DIR}/send/${file}" )"

	# Send Telegram message
	error_msg="$( curl -s --data-urlencode "text=$( echo -e "$msg" )" "https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&parse_mode=markdown" 2>&1 )"

	# Error checking
	if [ $? -eq 0 ]; then
		
		echo "[?] Message sent: '$file'"

		# Move message file to 'sent' directory
		mv "${DIR}/send/${file}" "${DIR}/sent/${file}"

		# Make sure it's removed from 'send' directory
		rm "${DIR}/send/${file}" 

		# Exit the program (program must send only one message at time)
		exit 0

	else

		echo "[!] Error: message could not be sent"
		echo "[!] Error message: ${error_msg}"

		# Exit program
		exit 1

	fi

done
