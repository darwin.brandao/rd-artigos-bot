#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

# Check if 'send' directory is empty. If true, empty sent dir, fetch new articles and generate new message files
[ -z "$( ls -A "${DIR}/send" )" ] && \
	"${DIR}/fetch-rss-feeds.sh" && \
	"${DIR}/gen-msg.sh" && \
	rm "${DIR}/sent/*"

# Send message
"${DIR}/send-msg.sh"
