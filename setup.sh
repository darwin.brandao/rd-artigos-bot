#!/bin/bash

packages=( 'docker' 'curl' 'jq' 'git' 'crontab' )

echo "[?] Checking if needed packages are installed"

# Check if packages are installed
for p in ${packages[@]}; do

	echo "[?] Package: $p"

	[ ! $(command -v "$p") ] && \
		echo "[!] $p is not installed. Please install it with your package manager" && \
		notInstalled = true
done

[ $notInstalled ] && exit 1 || echo "[?] Needed packages are installed"

echo "[?] Installing RSS Bridge"

# Create container
docker create --name=rss-bridge --publish 3000:80 rssbridge/rss-bridge

echo "[?] Starting RSS Bridge container"

# Start container
docker start rss-bridge

echo "[?] Downloading Darwin Custom RSS Bridges"

# Custom bridges
git clone https://gitlab.com/darwin.brandao/custom-rss-bridges.git

echo "[?] Installing custom bridges"

# Run auto install script for custom bridges
cd custom-rss-bridges
./auto-install-extensions.sh
cd ..

echo "[?] Setting up crontab"

# Crontab setup
(crontab -l 2>/dev/null; echo "*/10 * * * * bash $( pwd )/bot-man.sh") | crontab -
