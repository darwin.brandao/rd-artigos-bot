#!/bin/bash

# Functions
function log()
{
	[ "$DEBUG" = true ] && echo "$1"
}

function gen_md5()
{
	echo -n "$1" | md5sum | cut -d ' ' -f1
}
