#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

source "${DIR}/utils.sh"

for feed in "${DIR}"/feeds/*; do

	# Override $feed
	feed="$( basename "$feed" )"

	# Skip 'index' file
	[ "$feed" == "index" ] && continue

	# Get json
	json="$( cat "${DIR}/feeds/${feed}" )"
	
	# Loop RSS items
	while read -r item; do
	
		# Check if item exists
		[ -z "$item" ] && break
	
		# Get item data
		title="$( jq -r .title <<< $item )"
		url="$( jq -r .url <<< $item )"
	
		# Create message
		msg=$( 
cat <<EOF
🗞️ *${title}*

🔗 Link: ${url}

📨 RSS: $( grep "$feed" "${DIR}/feeds/index" | cut -f1 )
EOF
		)
	
		# Debug
		log "$msg"
	
		# Make 'send' directory
		mkdir -p "${DIR}/send"
	
		# Save message to 'send' directory
		echo "$msg" > "$DIR/send/$( gen_md5 $url )"
	
	done <<< "$( jq -c .items[] <<< "$json" )"
done
