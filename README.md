# rd-artigos-bot

This is a Telegram bot I made to fetch new scientific articles about Neuroscience, Psychology and Psychiatry from the RSS feeds of known scientific journals (you can check them all in 'config/rss-feeds') and send notifications about them to a public channel.

Video explaining how it works (Brazilian Portuguese): https://youtu.be/JoO5DItH7YA

You can see it in action in the following Telegram Channel link: https://t.me/rdartigos
Or search for "RD - Artigos Científicos" on Telegram.

- 'fetch-rss-feeds.sh'

It fetches the provided RSS feeds in 'config/rss-feeds' using RSS-Bridge and my custom bridge (FilterDWBridge.php), and save the raw data to the 'feeds' directory in a JSON format (RSS-Bridge converts XML to JSON).

- 'gen-msg.sh'

It loops the 'feeds' directory, and creates a message file for each article in the feed. The message contains its title, link and the RSS feed url. The message files are saved to a directory called 'send'.

- 'send-msg.sh'

It loops the 'send' directory, checks if the message file wasn't already sent (check if a file with the same name exists in the 'sent' directory), then it sends the message to the Chat specified in the 'chat\_id' variable in 'config/bot.cfg/' file. Finally, it moves the message file from 'send' to 'sent' directories and break out of the loop (it's supposed to only send 1 message per execution).

## Making it work:

- Change 'config/bot.cfg'
- Install RSS-Bridge via docker
- Add my custom bridges extensions
- Add command to your crontab, to run the bot every N minutes

It's important to notice that this is a BASH project. It runs on any Unix/Unix-Like systems, such as Linux, MacOS, BSDs, etc. I think you can run it on Windows Subsystem for Linux (WSL or WSL2), but I'm not sure.

## Config

You must change the 'config/bot.cfg' file, adding your Telegram Bot Token and Chat ID (the ID of your public channel, not its name or link, it's the ID, a numeric value).

bot.cfg:
```
token="YOUR_BOT_TOKEN_HERE"
chat_id="DESIRED_CHAT_ID_HERE"
```

## Dependencies

### RSS-Bridge

It needs a local [RSS-Bridge](https://github.com/RSS-Bridge/rss-bridge) instance running, and a custom bridge I made to get only today's RSS entries.

#### [Install with Docker:](https://github.com/RSS-Bridge/rss-bridge#install-with-docker)

Install by using docker image from Docker Hub:

```
# Create container
docker create --name=rss-bridge --publish 3000:80 rssbridge/rss-bridge

# Start container
docker start rss-bridge
```

Browse http://localhost:3000/

### My custom bridge (FilterDWBridge.php)

I modified the original FilterBridge.php (from RSS-Bridge) to let me only fetch the articles from today, ignoring all other articles.

Download my custom bridges at https://gitlab.com/darwin.brandao/custom-rss-bridges/ and run the 'auto-install-extensions.sh' script, to automatically copy my custom bridges to your RSS-Bridge docker instance and enable them.

## Crontab

I use crontab to run 'bot-man.sh' script every 10 minutes. 'bot-man.sh' is the Bot Manager script.

```
*/10 * * * * bash /home/your_user/path/to/project/rd-artigos-bot/bot-man.sh
```

Make sure to change the path of the script before adding this to your crontab.

## License

MIT, as always...
