#!/bin/bash

DEBUG=true

# Store the script path in a variable
DIR="$( cd -- "$(dirname "$( readlink -f "$0" )")" >/dev/null 2>&1 ; pwd -P )"

# Load utils.sh
source "${DIR}/utils.sh"

log "DIR: $DIR"

source "${DIR}/utils.sh"

# Make 'raw' directory
mkdir -p "${DIR}/feeds"
rm "${DIR}"/feeds/*

# Read RSS feeds file
while read -r feed; do

	log "Current feed: $feed"

	# URL Encode feed url
	encoded=$( "${DIR}"/urlencode $feed )

	# cURL to RSS Bridge instance
	json="$( curl -s "localhost:3000/?action=display&bridge=FilterDWBridge&url=${encoded}&today_items=on&relative_timezone=America%2FSao_Paulo&format=Json" )"

	# Save json to file
	filename="$( gen_md5 $feed )"
	echo "$json" > "${DIR}/feeds/${filename}"

	# Add entry to the index
	echo -e "${feed}\t${filename}" >> "${DIR}/feeds/index"

done <<< $( cat "${DIR}/config/rss-feeds" )
